+++
title = "Call for Diversity Scholarship Applications"
date = "2019-11-18T09:00:00"
tags = ["Scholarships", "DEI"]
categories = ["Scholarships"]
banner = "news/user2020-stl.png"
+++

![useR!2020 logo](/news/user2020-stl.png "useR!2020 logo")

useR! 2020 is proud to announce the diversity scholarship program. The program aims to alleviate the financial burden of attending useR! 2020 for members of underrepresented or historically marginalized groups in the R community who may not otherwise be able to attend the conference for financial reasons. 

## Diversity Scholarship benefits
- Reimbursement of eligible conference expenses
- Full registration ticket, including two tutorials and the gala dinner events
- Diversity Scholars welcome event
- Diversity Scholars social and networking event with conference sponsors


## Review criteria
**Note**: for more information please see link to ‘Tips for Applicants’ below  

- Underrepresented or historically marginalized groups in the R community who may not otherwise be able to attend our conference for financial reasons.
- Give evidence of your commitment to the field/community, e.g., narrative description and/or links to public evidence of activities and involvement. 
- Explain how you will pay it forward
- Describe your personal journey as an R user


## Applications deadline January 15  
Awards announced week of February 15


## Tips for Applicants & FAQs  
Diversity Scholarship applicants should wait to register until after the announcement of DS awards (around Feb 15) because registration fees will be waived for awardees.  (Note: The early bird registration rates are available until April 6.)"
https://tinyurl.com/useR2020-DS


## Submit your application
- Please read [Tips to Applicants and FAQs](https://tinyurl.com/useR2020-DS) before preparing your application
- Provide concise, specific, and clear responses to all application questions.
- Link to Application: https://tinyurl.com/useR2020-DS-apply
- First, register for an account (free) on the application portal. You can save your application and continue later by logging in using your registered email address and password.

**Note**: If you have difficulties logging in, please clear your web browser cookies, or try an alternative web browser.

### Questions not addressed in Tips for Applicants or FAQs?
Please email <a href="mailto:useR2020-dei@slu.edu?subject=useR 2020 Diversity Scholarship">useR2020-dei@slu.edu</a> and include “useR 2020 Diversity Scholarship” in the subject line of the message. 

### Data access and storage
All application information will be available only for the diversity scholarship committee and reviewers, and will be kept strictly confidential. 
Data will be collected using the platform OpenWater. OpenWater’s [data privacy and security information](https://www.getopenwater.com/the-company/security/).
