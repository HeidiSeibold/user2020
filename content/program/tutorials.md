# Online Tutorial Sessions 

- **Application of Gaussian graphical models to metabolomics** &mdash; [D. Scholtens](https://www.feinberg.northwestern.edu/faculty-profiles/az/profile.html?xid=16163) and [R. Balasubramanian](https://www.umass.edu/sphhs/person/faculty/raji-balasubramanian)
- **Causal inference in R** &mdash; [L. D'Agostino McGowan](https://www.lucymcgowan.com/) and [M. Barrett](https://malco.io/)
- **Create and share reproducible code with R Markdown and workflowr** &mdash; [J. Blischak](https://jdblischak.com/)
- **Creating beautiful data visualizations in R: a ggplot2 crash course** &mdash; [S. Tyner](https://sctyner.github.io/)
- **Easy Larger-than-RAM data manipulation with disk.frame** &mdash; [ZJ Dai](https://github.com/xiaodaigh)
- **End-to-end machine learning with Metaflow: Going from prototype to production with Netflix’s open source project for reproducible data science** &mdash; [S. Goyal](https://github.com/savingoyal) and B. Galvin
- **First steps in spatial data handling and visualization** &mdash; [S. Rochette](https://github.com/statnmap), [D. Scott](https://dscott.netlify.com/), and [J. Nowosad](https://nowosad.github.io/)
- **Getting the most out of Git** &mdash; [C. Gillespie](https://github.com/csgillespie) and [R. Davies](https://trianglegirl.rbind.io/)
- **Periscope and CanvasXpress – Creating an enterprise-grade big-data visualization application in a day** &mdash; [C. Brett](https://github.com/cb4ds)
- **Predictive modeling with text using tidy data principles** &mdash; [J. Silge](https://juliasilge.com/) and [E. Hvitfeldt](https://www.hvitfeldt.me/blog/)
- **Reproducible computation at scale with drake: hands-on practice with a machine learning project** &mdash; [W. Landau](https://wlandau.github.io/) 
- **Seamless R and C++ integration with Rcpp** &mdash; [D. Eddelbuettel](http://dirk.eddelbuettel.com/)
